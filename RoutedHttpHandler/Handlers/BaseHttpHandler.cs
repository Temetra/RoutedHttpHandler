﻿using RoutedHttpHandlerExample.Formatting;
using System;
using System.Threading.Tasks;
using System.Web;

namespace RoutedHttpHandlerExample.Handlers
{
	public abstract class BaseHttpHandler : HttpTaskAsyncHandler
	{
		protected IFormatterProvider Formatters { get; private set; }

		protected BaseHttpHandler(IFormatterProvider contentFormatter)
		{
			if (contentFormatter == null || contentFormatter.Count() < 1)
				throw new HttpException(500, "No content formatters configured");

			Formatters = contentFormatter;
		}

		public override bool IsReusable
		{
			get { return true; }
		}

		public override Task ProcessRequestAsync(HttpContext context)
		{
			HttpContextBase wrappedContext = new HttpContextWrapper(context);
			return ProcessRequestAsync(wrappedContext);
		}

		public abstract Task ProcessRequestAsync(HttpContextBase context);
	}
}