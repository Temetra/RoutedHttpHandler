﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RoutedHttpHandlerExample.Formatting
{
	public class FormatterMatch : IFormatter
	{
		private IFormatter formatter;
		private IMediaType matchingMediaType;

		public FormatterMatch(IFormatter formatter, IMediaType matchingMediaType)
		{
			this.formatter = formatter;
			this.matchingMediaType = matchingMediaType;
		}

		public IMediaType ContentType
		{
			get { return matchingMediaType; }
		}

		public ICollection<IMediaType> SupportedMediaTypes
		{
			get { return formatter.SupportedMediaTypes; }
		}

		public Task WriteAsync<T>(T value, Stream stream)
		{
			return formatter.WriteAsync(value, stream);
		}

		public Task<T> ReadAsync<T>(Stream stream)
		{
			return formatter.ReadAsync<T>(stream);
		}
	}
}
