﻿using System;
using System.Collections.Generic;

namespace RoutedHttpHandlerExample.Formatting
{
	public class MediaAcceptTypeComparer : IComparer<MediaAcceptType>
	{
		public int Compare(MediaAcceptType x, MediaAcceptType y)
		{
			// Validate input
			if (x == null) throw new ArgumentNullException("x");
			if (y == null) throw new ArgumentNullException("y");

			// Compare q value
			double xq = x.QValue.HasValue ? x.QValue.Value : 1.0;
			double yq = y.QValue.HasValue ? y.QValue.Value : 1.0;
			if (xq > yq) return 1;
			if (yq > xq) return -1;

			// Compare type
			if (string.Equals(x.RangeType, y.RangeType, StringComparison.OrdinalIgnoreCase))
			{
				// Compare subtype
				if (string.Equals(x.RangeSubtype, y.RangeSubtype, StringComparison.OrdinalIgnoreCase))
					return 0;

				// Compare wildcard
				if (x.RangeSubtype == "*") return -1;
				if (y.RangeSubtype == "*") return 1;
			}
			else
			{
				// Compare wildcard
				if (x.RangeType == "*") return -1;
				if (y.RangeType == "*") return 1;
				if (x.RangeSubtype == "*" && y.RangeSubtype != "*") return -1;
				if (x.RangeSubtype != "*" && y.RangeSubtype == "*") return 1;
			}

			return 0;
		}
	}
}
