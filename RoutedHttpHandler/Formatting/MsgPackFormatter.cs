﻿using MsgPack.Serialization;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RoutedHttpHandlerExample.Formatting
{
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Msg")]
	public class MsgPackFormatter : IFormatter
	{
		private static readonly MediaType[] supportedTypes = new MediaType[] { 
			new MediaType { RangeType = "application", RangeSubtype = "x-msgpack" } 
		};

		public IMediaType ContentType
		{
			get { return supportedTypes[0]; }
		}

		public ICollection<IMediaType> SupportedMediaTypes
		{
			get { return supportedTypes; }
		}

		public Task WriteAsync<T>(T value, Stream stream)
		{
			var serializer = MessagePackSerializer.Create<T>();
			serializer.Pack(stream, value);
			return Task.FromResult(true);
		}

		public Task<T> ReadAsync<T>(Stream stream)
		{
			var serializer = MessagePackSerializer.Create<T>();
			var value = serializer.Unpack(stream);
			return Task.FromResult(value);
		}
	}
}