﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RoutedHttpHandlerExample.Formatting
{
	public class FormatterProvider : IFormatterProvider
	{
		private List<IFormatter> formatters;

		public FormatterProvider()
		{
			formatters = new List<IFormatter>();
		}

		public void Add(IFormatter formatter)
		{
			formatters.Add(formatter);
		}

		public int Count()
		{
			return formatters.Count;
		}

		public IFormatter SelectFirst()
		{
			return formatters.First();
		}

		public IFormatter SelectFirstMatch(IMediaType acceptedType)
		{
			// Validate input
			if (acceptedType == null) return null;

			// Get formatter
			foreach (var formatter in this.formatters)
			{
				foreach (var formatterMediaType in formatter.SupportedMediaTypes)
				{
					if (acceptedType.RangeType == "*" || acceptedType.RangeType.Equals(formatterMediaType.RangeType, StringComparison.OrdinalIgnoreCase))
					{
						if (acceptedType.RangeSubtype == "*" || acceptedType.RangeSubtype.Equals(formatterMediaType.RangeSubtype, StringComparison.OrdinalIgnoreCase))
							return new FormatterMatch(formatter, formatterMediaType);
					}
				}
			}

			return null;
		}

		public IFormatter SelectFirstInRange(IEnumerable<IMediaType> acceptedTypes)
		{
			// Check accepted types
			if (acceptedTypes == null || acceptedTypes.Count() < 1) return null;

			// Check we have formatters
			if (this.Count() < 1) return null;

			// Search collection for formatter that matches the first accepted type
			IFormatter result = null;

			foreach (var acceptedMediaType in acceptedTypes)
			{
				result = SelectFirstMatch(acceptedMediaType);
				if (result != null) return result;
			}

			// Failed to find a formatter
			return null;
		}
	}
}
