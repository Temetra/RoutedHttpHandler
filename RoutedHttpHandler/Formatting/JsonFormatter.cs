﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RoutedHttpHandlerExample.Formatting
{
	public class JsonFormatter : IFormatter
	{
		private static readonly MediaType[] supportedTypes = new MediaType[] { 
			new MediaType { RangeType = "application", RangeSubtype = "json" } 
		};

		public IMediaType ContentType
		{
			get { return supportedTypes[0]; }
		}

		public ICollection<IMediaType> SupportedMediaTypes
		{
			get { return supportedTypes; }
		}
		
		public Task WriteAsync<T>(T value, Stream stream)
		{
			ServiceStack.Text.JsonSerializer.SerializeToStream(value, stream);
			return Task.FromResult(true);
		}

		public Task<T> ReadAsync<T>(Stream stream)
		{
			var value = ServiceStack.Text.JsonSerializer.DeserializeFromStream<T>(stream);
			return Task.FromResult(value);
		}
	}
}