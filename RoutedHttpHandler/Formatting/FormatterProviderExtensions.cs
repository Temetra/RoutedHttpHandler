﻿using System;
using System.Web;

namespace RoutedHttpHandlerExample.Formatting
{
	public static class FormatterProviderExtensions
	{
		public static IFormatter GetRequestFormatter(this IFormatterProvider provider, string contentType)
		{
			if (string.IsNullOrEmpty(contentType)) return null;

			IFormatter formatter = null;

			// Convert content type header to MediaType
			MediaType mediaType = MediaType.Parse(contentType);

			// Get formatter for processing request
			if (mediaType != null) formatter = provider.SelectFirstMatch(mediaType);
			if (formatter == null) throw new HttpException(415, "Unsupported Media Type");

			// Return
			return formatter;
		}

		public static IFormatter GetResponseFormatter(this IFormatterProvider provider, string[] acceptedTypes, Func<IFormatter> fallback)
		{
			if (acceptedTypes == null || acceptedTypes.Length < 1)
			{
				if (fallback == null) return null;
				return fallback();
			}

			IFormatter formatter = null;

			// Convert accept header to sorted enumerable
			var parsedAcceptedTypes = MediaAcceptType.Parse(acceptedTypes);

			// Find acceptable formatter
			formatter = provider.SelectFirstInRange(parsedAcceptedTypes);
			if (formatter == null) throw new HttpException(406, "Not Acceptable");

			// Return
			return formatter;
		}
	}
}