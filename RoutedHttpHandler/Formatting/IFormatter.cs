﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RoutedHttpHandlerExample.Formatting
{
	public interface IFormatter
	{
		IMediaType ContentType { get; }
		ICollection<IMediaType> SupportedMediaTypes { get; }
		Task WriteAsync<T>(T value, Stream stream);
		Task<T> ReadAsync<T>(Stream stream);
	}
}
