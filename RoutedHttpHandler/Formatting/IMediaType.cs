﻿
namespace RoutedHttpHandlerExample.Formatting
{
	public interface IMediaType
	{
		string RangeType { get; set; }
		string RangeSubtype { get; set; }
	}
}