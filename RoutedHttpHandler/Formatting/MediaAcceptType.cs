﻿using System.Linq;

namespace RoutedHttpHandlerExample.Formatting
{
	public class MediaAcceptType : IMediaType
	{
		private static readonly MediaAcceptTypeComparer MediaAcceptTypeComparer = new MediaAcceptTypeComparer();

		public string RangeType { get; set; }
		public string RangeSubtype { get; set; }
		public double? QValue { get; set; }

		/// <returns>Input as MediaAcceptType, or null if parse failed</returns>
		public static MediaAcceptType Parse(string input)
		{
			// Check input type
			if (string.IsNullOrEmpty(input)) return null;

			// Split request header field
			var a = input.Split('/', ';');

			// Check split
			if (a.Length < 2) return null;

			// Create media type
			var output = new MediaAcceptType
			{
				RangeType = a[0],
				RangeSubtype = a[1]
			};

			// Find q value in optional params
			int i = 2;
			string[] acceptParam;
			double parseResult;
			while (i < a.Length)
			{
				acceptParam = a[i].Split('=');
				if (acceptParam[0] == "q" && double.TryParse(acceptParam[1], out parseResult)) output.QValue = parseResult;
				i++;
			}

			// Return
			return output;
		}

		/// <returns>Input as sorted enumerable</returns>
		public static IOrderedEnumerable<MediaAcceptType> Parse(string[] input)
		{
			return input
				.Select(x => MediaAcceptType.Parse(x))
				.Where(x => x != null)
				.OrderByDescending((k) => k, MediaAcceptType.MediaAcceptTypeComparer);
		}
	}
}
