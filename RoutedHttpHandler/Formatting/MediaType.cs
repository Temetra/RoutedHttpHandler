﻿
namespace RoutedHttpHandlerExample.Formatting
{
	public class MediaType : IMediaType
	{
		public string RangeType { get; set; }
		public string RangeSubtype { get; set; }

		public override string ToString()
		{
			return RangeType + "/" + RangeSubtype;
		}

		/// <returns>Input as MediaType, or null if parse failed</returns>
		public static MediaType Parse(string input)
		{
			// Check accepted type
			if (string.IsNullOrEmpty(input)) return null;

			// Split type into Type/SubType
			var a = input.Split('/');

			// Check split
			if (a.Length != 2) return null;

			// Return
			return new MediaType { RangeType = a[0], RangeSubtype = a[1] };
		}
	}
}
