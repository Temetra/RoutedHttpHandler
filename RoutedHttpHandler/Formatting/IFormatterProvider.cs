﻿using System.Collections.Generic;

namespace RoutedHttpHandlerExample.Formatting
{
	public interface IFormatterProvider
	{
		int Count();
		void Add(IFormatter formatter);
		IFormatter SelectFirst();
		IFormatter SelectFirstMatch(IMediaType acceptedType);
		IFormatter SelectFirstInRange(IEnumerable<IMediaType> acceptedTypes);
	}
}