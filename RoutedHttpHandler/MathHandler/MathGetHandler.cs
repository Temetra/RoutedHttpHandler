﻿using RoutedHttpHandlerExample.Formatting;
using RoutedHttpHandlerExample.Handlers;
using RoutedHttpHandlerExample.Models;
using System;
using System.Threading.Tasks;
using System.Web;

namespace RoutedHttpHandlerExample.MathHandler
{
	public interface IMathGetHandler : IHttpHandler { }
	
	public class MathGetHandler : BaseHttpHandler, IMathGetHandler
	{
		public MathGetHandler(IFormatterProvider formatterProvider)
			: base(formatterProvider)
		{
		}

		public override async Task ProcessRequestAsync(HttpContextBase context)
		{
			// Validate input
			if (context == null) throw new ArgumentNullException("context");

			// Get formatter
			IFormatter outputFormatter = Formatters.GetResponseFormatter(context.Request.AcceptTypes, Formatters.SelectFirst);

			// Output sample request
			MathRequest request = new MathRequest { Left = 4, Right = 7, Operation = MathOperation.Add };

			// Begin output
			Task responseTask = outputFormatter.WriteAsync(request, context.Response.OutputStream);

			// Set cacheability of request
			context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
			context.Response.Cache.SetNoStore();
			context.Response.Cache.SetExpires(DateTime.MinValue);

			// Output response
			context.Response.StatusCode = 200;
			context.Response.StatusDescription = "OK";
			context.Response.ContentType = outputFormatter.ContentType.ToString();

			// Wait for output to finish
			await responseTask;
		}
	}
}