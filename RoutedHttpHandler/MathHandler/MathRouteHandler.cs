﻿using RoutedHttpHandlerExample.Handlers;
using System;
using System.Web;
using System.Web.Routing;

namespace RoutedHttpHandlerExample.MathHandler
{
	public delegate IMathGetHandler MathGetHandlerFactory();
	public delegate IMathPostHandler MathPostHandlerFactory();

	public class MathRouteHandler : IRouteHandler
	{
		private MathGetHandlerFactory getFactory;
		private MathPostHandlerFactory postFactory;

		public MathRouteHandler(MathGetHandlerFactory getFactory, MathPostHandlerFactory postFactory)
		{
			this.getFactory = getFactory;
			this.postFactory = postFactory;
		}

		public IHttpHandler GetHttpHandler(RequestContext requestContext)
		{
			if (requestContext == null) throw new ArgumentNullException("requestContext");
			if (requestContext.HttpContext == null) throw new ArgumentNullException("requestContext");
			if (requestContext.HttpContext.Request == null) throw new ArgumentNullException("requestContext");
			return GetHttpHandler(requestContext.HttpContext.Request.HttpMethod);
		}

		public IHttpHandler GetHttpHandler(string httpMethod)
		{
			if (httpMethod == null) throw new ArgumentNullException("httpMethod");
			IHttpHandler handler = null;

			// GET handler
			if (string.Equals("get", httpMethod, StringComparison.OrdinalIgnoreCase))
				handler = getFactory();

			// POST handler
			if (string.Equals("post", httpMethod, StringComparison.OrdinalIgnoreCase))
				handler = postFactory();

			// Return handler
			if (handler == null) throw new HttpException(404, "Not Found");
			else return handler;
		}
	}
}