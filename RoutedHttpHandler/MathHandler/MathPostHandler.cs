﻿using RoutedHttpHandlerExample.Formatting;
using RoutedHttpHandlerExample.Handlers;
using RoutedHttpHandlerExample.Models;
using System;
using System.Threading.Tasks;
using System.Web;

namespace RoutedHttpHandlerExample.MathHandler
{
	public interface IMathPostHandler : IHttpHandler { }
	
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "PostProcessor")]
	public class MathPostHandler : BaseHttpHandler, IMathPostHandler
	{
		public MathPostHandler(IFormatterProvider formatterProvider)
			: base(formatterProvider)
		{
		}

		public override async Task ProcessRequestAsync(HttpContextBase context)
		{
			// Validate input
			if (context == null) throw new ArgumentNullException("context");

			// Get formatters
			IFormatter inputFormatter = Formatters.GetRequestFormatter(context.Request.ContentType);
			IFormatter outputFormatter = Formatters.GetResponseFormatter(context.Request.AcceptTypes, () => inputFormatter);

			// Wait for formatter read task
			var request = await inputFormatter.ReadAsync<MathRequest>(context.Request.InputStream);

			// Create response
			MathResponse mathResponse = new MathResponse
			{
				Left = request.Left,
				Right = request.Right,
				Operation = request.Operation
			};

			mathResponse.CalculateAnswer();

			// Begin output
			var responseTask = outputFormatter.WriteAsync(mathResponse, context.Response.OutputStream);

			// Set cacheability of request
			context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
			context.Response.Cache.SetNoStore();
			context.Response.Cache.SetExpires(DateTime.MinValue);

			// Output response
			context.Response.StatusCode = 200;
			context.Response.StatusDescription = "OK";
			context.Response.ContentType = outputFormatter.ContentType.ToString();

			// Wait for output to finish
			await responseTask;
		}
	}
}