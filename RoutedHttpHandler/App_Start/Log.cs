﻿using System.Threading;

namespace RoutedHttpHandlerExample
{
	internal static class Log
	{
		private static readonly System.Diagnostics.TraceSource Trace = InitializeTrace();

		static System.Diagnostics.TraceSource InitializeTrace()
		{
			// Get assembly name
			var assembly = System.Reflection.Assembly.GetExecutingAssembly();
			var name = assembly.GetName().Name;

			// Create trace source
			var trace = new System.Diagnostics.TraceSource(name);

			// Remove default listener
			Trace.Listeners.Remove("Default");

			// Set source to null if there are no listeners
			if (Trace.Listeners.Count < 1) trace = null;

			return trace;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
		public static void Information(string text)
		{
			// Exit if there's no trace source or text
			if (Trace == null || string.IsNullOrEmpty(text)) return;

			// Trace info event
			Trace.TraceInformation("Thread={0}. {1}.", Thread.CurrentThread.ManagedThreadId, text);
		}
	}
}