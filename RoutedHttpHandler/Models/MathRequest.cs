﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoutedHttpHandlerExample.Models
{
	public class MathRequest
	{
		public double Left { get; set; }
		public double Right { get; set; }
		public MathOperation Operation { get; set; }
	}
}