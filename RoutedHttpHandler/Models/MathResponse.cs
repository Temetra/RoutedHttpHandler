﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoutedHttpHandlerExample.Models
{
	public class MathResponse : MathRequest
	{
		public double Result { get; set; }

		public void CalculateAnswer()
		{
			switch (Operation)
			{
				case MathOperation.Add:
					Result = Left + Right;
					break;
				case MathOperation.Subtract:
					Result = Left - Right;
					break;
				default:
					break;
			}
		}
	}
}