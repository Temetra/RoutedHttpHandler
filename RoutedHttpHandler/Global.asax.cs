﻿using RoutedHttpHandlerExample.Formatting;
using RoutedHttpHandlerExample.Handlers;
using RoutedHttpHandlerExample.MathHandler;
using System;
using System.Web.Routing;

namespace RoutedHttpHandlerExample
{
	public class Global : System.Web.HttpApplication
	{
		protected void Application_Start(object sender, EventArgs e)
		{
			var container = new SimpleInjector.Container();
			container.RegisterSingle<IFormatterProvider, FormatterProvider>();
			container.Register<MathGetHandlerFactory>(() => container.GetInstance<MathGetHandler>);
			container.Register<MathPostHandlerFactory>(() => container.GetInstance<MathPostHandler>);
			container.Verify();

			var contentFormatterCollection = container.GetInstance<IFormatterProvider>();
			contentFormatterCollection.Add(new JsonFormatter());
			contentFormatterCollection.Add(new MsgPackFormatter());

			RouteTable.Routes.Add(new Route(
				"",
				new RouteValueDictionary { },
				new System.Web.Routing.PageRouteHandler("~/Static/Index.html")
				));

			RouteTable.Routes.Add(new Route(
				"math",
				new RouteValueDictionary { },
				container.GetInstance<MathRouteHandler>()
				));
		}
	}
}