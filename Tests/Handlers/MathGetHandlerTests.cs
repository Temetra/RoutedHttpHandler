﻿using TestCategory = Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute;
using TestClass = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using TestMethod = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Moq;
using NUnit.Framework;

using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using RoutedHttpHandlerExample.Formatting;
using RoutedHttpHandlerExample.Handlers;
using RoutedHttpHandlerExample.MathHandler;
using RoutedHttpHandlerExample.Models;

namespace Tests.Formatting
{
	[TestClass]
	public class MathGetProcessorTests
	{
		[TestMethod, TestCategory("MathHandler")]
		public void SuccessfulRun()
		{
			// Formatter
			var formatter = CreateMockFormatter("Test message", "application/json");

			// Formatter provider
			IFormatterProvider provider = new FormatterProvider();
			provider.Add(formatter.Object);
			
			// Processor
			BaseHttpHandler processor = new MathGetHandler(provider);

			// Mock request
			var mockContext = new HttpContextMock();
			mockContext.RequestAcceptTypes = new string[] { "application/json" };

			// Execute
			processor.ProcessRequestAsync(mockContext.Context)
				.Wait();
			
			// Assert
			Assert.That(mockContext.CacheHttpCacheability, Is.EqualTo(System.Web.HttpCacheability.NoCache));
			Assert.That(mockContext.CacheNoStore, Is.True);
			Assert.That(mockContext.CacheExpires, Is.EqualTo(DateTime.MinValue));
			Assert.That(mockContext.Context.Response.StatusCode, Is.EqualTo(200));
			Assert.That(mockContext.Context.Response.StatusDescription, Is.EqualTo("OK"));
			Assert.That(mockContext.Context.Response.ContentType, Is.EqualTo("application/json"));
			var result = mockContext.ReadFromOutputStream();
			Assert.That(result, Is.EqualTo("Test message"));
		}

		private static Mock<IFormatter> CreateMockFormatter(string output, string contentType)
		{
			var formatter = new Mock<IFormatter>();
			var mediaType = MediaType.Parse(contentType);
			formatter.SetupGet(x => x.ContentType).Returns(mediaType);
			formatter.SetupGet(x => x.SupportedMediaTypes).Returns(new IMediaType[] { mediaType });

			formatter
				.Setup(x => x.WriteAsync(It.IsAny<MathRequest>(), It.IsAny<Stream>()))
				.Callback<MathRequest, Stream>((value, stream) =>
				{
					var buffer = Encoding.UTF8.GetBytes(output);
					stream.Write(buffer, 0, buffer.Length);
				})
				.Returns(Task.FromResult(true));

			return formatter;
		}
	}
}
