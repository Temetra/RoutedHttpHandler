﻿using TestCategory = Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute;
using TestClass = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using TestMethod = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Moq;
using NUnit.Framework;

using System;
using System.Web;
using System.Web.Routing;
using RoutedHttpHandlerExample.Handlers;
using RoutedHttpHandlerExample.MathHandler;
using RoutedHttpHandlerExample.Formatting;

namespace Tests.Handlers
{
	[TestClass]
	public class MathRouteHandlerTests
	{
		[TestMethod, TestCategory("MathHandler")]
		public void GetHandler()
		{
			// Set up
			var mockGetProcessor = new Mock<IMathGetHandler>();
			var mockPostProcessor = new Mock<IMathPostHandler>();
			MathGetHandlerFactory getResolver = () => mockGetProcessor.Object;
			MathPostHandlerFactory postResolver = () => mockPostProcessor.Object;
			var factory = new MathRouteHandler(getResolver, postResolver);

			// Execute and assert
			IHttpHandler getHandler = factory.GetHttpHandler("GET");
			Assert.That(getHandler, Is.EqualTo(mockGetProcessor.Object));

			// Execute and assert
			IHttpHandler postHandler = factory.GetHttpHandler("POST");
			Assert.That(postHandler, Is.EqualTo(mockPostProcessor.Object));
		}
	}
}
