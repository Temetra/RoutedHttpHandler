﻿using Moq;
using System;
using System.IO;
using System.Web;

namespace Tests
{
	public class HttpContextMock : IDisposable
	{
		private Mock<HttpContextBase> context;
		private Mock<HttpRequestBase> request;
		private Mock<HttpResponseBase> response;
		private Mock<HttpCachePolicyBase> cache;
		private Mock<HttpSessionStateBase> sessionState;
		private Mock<HttpApplicationStateBase> applicationState;

		public HttpContextBase Context { get { return context.Object; } }
		public MemoryStream InputStream { get; private set; }
		public MemoryStream OutputStream { get; private set; }

		public string[] RequestAcceptTypes { get; set; }
		public string RequestHttpMethod { get; set; }
		public int RequestContentLength { get; set; }
	
		public HttpCacheability CacheHttpCacheability { get; private set; }
		public bool CacheNoStore { get; private set; }
		public DateTime CacheExpires { get; private set; }

		public HttpContextMock()
		{
			InputStream = new MemoryStream();
			OutputStream = new MemoryStream();
			CacheNoStore = false;

			// Context mock
			context = new Mock<HttpContextBase>();
			request = new Mock<HttpRequestBase>();
			cache = new Mock<HttpCachePolicyBase>();
			response = new Mock<HttpResponseBase>();
			sessionState = new Mock<HttpSessionStateBase>();
			applicationState = new Mock<HttpApplicationStateBase>();

			context.SetupGet(x => x.Request).Returns(request.Object);
			context.SetupGet(x => x.Response).Returns(response.Object);
			context.SetupGet(x => x.Session).Returns(sessionState.Object);
			context.SetupGet(x => x.Application).Returns(applicationState.Object);
			response.SetupGet(x => x.Cache).Returns(cache.Object);

			// Request
			request.SetupGet(x => x.AcceptTypes).Returns(() => RequestAcceptTypes);
			request.SetupGet(x => x.HttpMethod).Returns(() => RequestHttpMethod);
			request.SetupProperty(x => x.ContentType);
			request.SetupGet(r => r.InputStream).Returns(() => InputStream);
			request.SetupGet(x => x.ContentLength).Returns(() => (int)InputStream.Length);

			// Cache
			cache.Setup(x => x.SetCacheability(It.IsAny<HttpCacheability>())).Callback<HttpCacheability>(value => CacheHttpCacheability = value);
			cache.Setup(x => x.SetNoStore()).Callback(() => CacheNoStore = true);
			cache.Setup(x => x.SetExpires(It.IsAny<DateTime>())).Callback<DateTime>(value => CacheExpires = value);

			// Response
			response.SetupProperty(x => x.StatusCode);
			response.SetupProperty(x => x.StatusDescription);
			response.SetupProperty(x => x.ContentType);
			response.SetupGet(x => x.OutputStream).Returns(OutputStream);
			response.Setup(x => x.Write(It.IsAny<string>()))
				.Callback<string>(s => 
				{
					var sr = new StreamWriter(OutputStream);
					sr.Write(s);
					sr.Flush();
				});
		}

		public string ReadFromOutputStream()
		{
			var originalPosition = OutputStream.Position;
			OutputStream.Position = 0;
			var reader = new System.IO.StreamReader(OutputStream);
			var text = reader.ReadToEnd();
			OutputStream.Position = originalPosition;
			return text;
		}

		public void WriteToInputStream(string input)
		{
			var writer = new System.IO.StreamWriter(InputStream);
			InputStream.Position = InputStream.Length;
			writer.Write(input);
			writer.Flush();
			InputStream.Position = 0;
		}

		#region Dispose
		bool disposed;

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		~HttpContextMock()
		{
			Dispose(false);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposed)
				return;

			if (disposing)
			{
				// free other managed objects that implement IDisposable only
				InputStream.Dispose();
				OutputStream.Dispose();
			}

			// release any unmanaged objects
			// set the object references to null
			InputStream = null;
			OutputStream = null;

			disposed = true;
		}
		#endregion
	}
}
