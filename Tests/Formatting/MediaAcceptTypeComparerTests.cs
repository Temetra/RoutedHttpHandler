﻿using TestClass = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using TestMethod = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using TestCategory = Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute;
using NUnit.Framework;
using RoutedHttpHandlerExample.Formatting;

namespace Tests.Formatting
{
	[TestClass]
	public class MediaAcceptTypeComparerTests
	{
		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void IdenticalTypes()
		{
			var first = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain", QValue = 1.0 };
			var second = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain", QValue = 1.0 };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(0));
		}

		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void DifferentTypes()
		{
			var first = new MediaAcceptType { RangeType = "nottext", RangeSubtype = "notplain", QValue = 1.0 };
			var second = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain", QValue = 1.0 };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(0));
		}

		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void DifferentSubTypes()
		{
			var first = new MediaAcceptType { RangeType = "text", RangeSubtype = "notplain", QValue = 1.0 };
			var second = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain", QValue = 1.0 };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(0));
		}

		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void IdenticalTypesNoQValues()
		{
			var first = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain" };
			var second = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain" };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(0));
		}

		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void DifferentTypesNoQValues()
		{
			var first = new MediaAcceptType { RangeType = "nottext", RangeSubtype = "notplain" };
			var second = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain" };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(0));
		}

		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void HighFirstQValue()
		{
			var first = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain", QValue = 1.0 };
			var second = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain", QValue = 0.5 };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(1));
		}

		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void HighSecondQValue()
		{
			var first = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain", QValue = 0.5 };
			var second = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain", QValue = 1.0 };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(-1));
		}

		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void FirstPartlyWild()
		{
			var first = new MediaAcceptType { RangeType = "text", RangeSubtype = "*", QValue = 1.0 };
			var second = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain", QValue = 1.0 };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(-1));
		}

		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void SecondPartlyWild()
		{
			var first = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain", QValue = 1.0 };
			var second = new MediaAcceptType { RangeType = "text", RangeSubtype = "*", QValue = 1.0 };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(1));
		}

		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void FirstFullyWild()
		{
			var first = new MediaAcceptType { RangeType = "*", RangeSubtype = "*", QValue = 1.0 };
			var second = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain", QValue = 1.0 };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(-1));
		}

		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void SecondFullyWild()
		{
			var first = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain", QValue = 1.0 };
			var second = new MediaAcceptType { RangeType = "*", RangeSubtype = "*", QValue = 1.0 };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(1));
		}

		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void DifferentFirstPartlyWild()
		{
			var first = new MediaAcceptType { RangeType = "text", RangeSubtype = "*", QValue = 1.0 };
			var second = new MediaAcceptType { RangeType = "nottext", RangeSubtype = "plain", QValue = 1.0 };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(-1));
		}

		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void DifferentSecondPartlyWild()
		{
			var first = new MediaAcceptType { RangeType = "text", RangeSubtype = "plain", QValue = 1.0 };
			var second = new MediaAcceptType { RangeType = "nottext", RangeSubtype = "*", QValue = 1.0 };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(1));
		}

		[TestMethod, TestCategory("MediaAcceptTypeComparer")]
		public void DifferentPartlyWild()
		{
			var first = new MediaAcceptType { RangeType = "text", RangeSubtype = "*", QValue = 1.0 };
			var second = new MediaAcceptType { RangeType = "nottext", RangeSubtype = "*", QValue = 1.0 };
			var comparer = new MediaAcceptTypeComparer();
			var result = comparer.Compare(first, second);
			Assert.That(result, Is.EqualTo(0));
		}
	}
}
